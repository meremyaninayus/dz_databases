﻿using Dapper;
using DataBaseReader.Models;
using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataBaseReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var users = DataBaseHelper.DapperSelectUsers();
            Console.WriteLine("Все пользователи");
            foreach (var user in users)
            {
                Console.WriteLine($"{user.UserName} - {user.Email}");
            }
            var categories = DataBaseHelper.DapperSelectCategories();
            Console.WriteLine("Все категории");
            foreach (var category in categories)
            {
                Console.WriteLine($"{category.CategoryName}");
            }
            var advertisements = DataBaseHelper.DapperSelectAds();
            Console.WriteLine("Все объявления");
            foreach (var ad in advertisements)
            {
                Console.WriteLine($"{ad.Category.CategoryName} - {ad.User.UserName} - {ad.AdvertisementText}");
            }
            Console.WriteLine("Введите название для новой категории");
            var categoryName = Console.ReadLine();
            DataBaseHelper.DapperInsertCategory(new Category { CategoryName = categoryName });
            categories = DataBaseHelper.DapperSelectCategories();
            Console.WriteLine("Все категории");
            foreach (var category in categories)
            {
                Console.WriteLine($"{category.CategoryName}");
            }
        }
    }
}
