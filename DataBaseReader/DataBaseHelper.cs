﻿using Dapper;
using DataBaseReader.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataBaseReader
{
    public static class DataBaseHelper
    {
        const string connectionString = "Host=localhost;Username=myUser;Password=123;Database=Avito";
        public static IEnumerable<User> DapperSelectUsers()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                var userDictionary = new Dictionary<int, User>();
                connection.Open();
                IEnumerable<User> users = connection.Query<User, Advertisement, User>(@"select 
u.user_id UserId,
u.user_name UserName,
u.user_password UserPassword,
email Email,
a.advertisement_id AdvertisementId, 
a.user_id UserId, 
a.ad_text AdvertisementText, 
a.category_id CategoryId, 
a.publicatetion_date PublicatetionDate
from 
users u
left join
advertisements a
on u.user_id=a.user_id", (user, ad) =>
                {
                    User userEntry;
                    if (!userDictionary.TryGetValue(user.UserId, out userEntry))
                    {
                        userEntry = user;
                        userEntry.Advertisements = new List<Advertisement>();
                        userDictionary.Add(userEntry.UserId, userEntry);
                    }
                    if (ad != null)
                        userEntry.Advertisements.Add(ad);
                    return userEntry;
                }, splitOn: "AdvertisementId").Distinct();
                return users;
            }
        }
        public static IEnumerable<Category> DapperSelectCategories()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                var categoryDictionary = new Dictionary<int, Category>();
                connection.Open();
                IEnumerable<Category> categories = connection.Query<Category, Advertisement, Category>(@"select 
c.category_id CategoryId,
c.category_name CategoryName,
a.advertisement_id AdvertisementId, 
a.user_id UserId, 
a.ad_text AdvertisementText, 
a.category_id CategoryId, 
a.publicatetion_date PublicatetionDate
from 
categories c
left join
advertisements a
on c.category_id=a.category_id"
                , (category, ad) =>
                {
                    Category categoryEntry;

                    if (!categoryDictionary.TryGetValue(category.CategoryId, out categoryEntry))
                    {
                        categoryEntry = category;
                        categoryEntry.Advertisements = new List<Advertisement>();
                        categoryDictionary.Add(categoryEntry.CategoryId, categoryEntry);
                    }
                    if (ad != null)
                        categoryEntry.Advertisements.Add(ad);
                    return categoryEntry;
                }, splitOn: "AdvertisementId").Distinct();

                return categories;
            }
        }

        public static IEnumerable<Advertisement> DapperSelectAds()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                IEnumerable<Advertisement> advertisements = connection.Query<Advertisement, Category, User, Advertisement>(@"select
a.advertisement_id AdvertisementId, 
a.user_id UserId, 
a.ad_text AdvertisementText, 
a.category_id CategoryId, 
a.publicatetion_date PublicatetionDate,
c.category_id CategoryId,
c.category_name CategoryName,
u.user_id UserId,
u.user_name UserName,
u.user_password UserPassword,
email Email
FROM advertisements a
INNER JOIN
categories c on a.category_id = c.category_id
INNER JOIN
users u on a.user_id = u.user_id",
                (advertisement, category, user)=>
                                {
                                    advertisement.Category = category;
                                    advertisement.User = user;
                                    return advertisement;
                                }
                                , splitOn: "AdvertisementId,CategoryId,UserId");
                return advertisements;
            }
        }

        public static void DapperInsertCategory(Category category)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    connection.Query<long>(@"INSERT INTO categories(category_name) 
                                            VALUES(@CategoryName)
                                            RETURNING category_id",
                    category,
                    transaction);
                    transaction.Commit();
                }
            }
         }
    }
}
