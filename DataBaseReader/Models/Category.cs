﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseReader.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<Advertisement> Advertisements { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.GetType()!=typeof(Category))
                return false;
            if (((Category)obj).CategoryId == this.CategoryId)
                return true;
            return false;
        }
    }
}
