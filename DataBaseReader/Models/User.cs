﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseReader.Models
{
    public class User
    {
        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string Email { get; set; }
        public List<Advertisement> Advertisements { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(User))
                return false;
            if (((User)obj).UserId == this.UserId)
                return true;
            return false;
        }
    }
}
