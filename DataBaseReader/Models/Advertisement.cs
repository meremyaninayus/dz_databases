﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseReader.Models
{
    public class Advertisement
    {
        public User User { get; set; }
        public string AdvertisementText { get; set; }
        public Category Category { get; set; }
        public DateTime PublicationDate { get; set; }
        public int AdvertisementId { get; set; }
    }
}
