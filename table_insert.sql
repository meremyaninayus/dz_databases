INSERT INTO users (user_name, user_password, email)
VALUES
('Kotik', '12345', 'cat@mail.ru'),
('Jane', '12345', 'jane@mail.ru'),
('Max', '12345', 'max@mail.ru'),
('NightWalker', '12345', 'nv@mail.ru'),
('ninja', '12345', 'ninja@mail.ru');

INSERT INTO categories (category_name)
VALUES
('Детские вещи'),
('Недвижимость'),
('Домашние животные'),
('Спортивные товары'),
('Комнатные растения');

INSERT INTO advertisements (user_id, ad_text, category_id, publicatetion_date)
VALUES
(1,'Продам фикус', 5, current_date),
(2,'Отдам котенка', 3, current_date),
(3,'Куплю дом', 2, current_date),
(5,'Продам гантели', 4, current_date),
(2,'Продам беговую дорожку', 4, current_date);