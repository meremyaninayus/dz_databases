CREATE SEQUENCE user_ids start 1;

CREATE TABLE users (
     user_id    integer PRIMARY KEY DEFAULT nextval('user_ids'),
     user_name   varchar(50) NOT NULL CHECK (user_name <> ''),
	 user_password   varchar(15) NOT NULL CHECK (user_password <> ''),
	 email   varchar(150) NOT NULL CHECK (email <> '')
);

CREATE SEQUENCE category_ids start 1;

CREATE TABLE categories (
     category_id    integer PRIMARY KEY DEFAULT nextval('category_ids'),
     category_name   varchar(50) NOT NULL CHECK (category_name <> '')
);

CREATE SEQUENCE advertisement_ids start 1;

CREATE TABLE advertisements (
	 advertisement_id integer PRIMARY KEY DEFAULT nextval('advertisement_ids'),
	 user_id integer,
     FOREIGN KEY (user_id) REFERENCES users (user_id) ,
     ad_text   varchar(500) NOT NULL CHECK (ad_text <> ''),
	 category_id integer,
	 FOREIGN KEY (category_id) REFERENCES categories (category_id),
	 publicatetion_date date
);
